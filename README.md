[![Codeship Status](https://codeship.com/projects/1ca8e410-bead-0133-f9f4-6e90c22d1400/status?branch=master)](https://codeship.com/projects/136971)
[![Code Climate](https://codeclimate.com/repos/56d04195d135494bfd007f6f/badges/82069723e476edafd076/gpa.svg)](https://codeclimate.com/repos/56d04195d135494bfd007f6f/feed)
[![Issue Count](https://codeclimate.com/repos/56d04195d135494bfd007f6f/badges/82069723e476edafd076/issue_count.svg)](https://codeclimate.com/repos/56d04195d135494bfd007f6f/feed)

# Dispo API

This README outlines the details of collaborating on this Rails application.
A short introduction of this app could easily go here.

## Prerequisites

You will need the following things properly installed on your computer.

* [Git](http://git-scm.com/)
* [Node.js](http://nodejs.org/) (with NPM)

## Installation

* `git clone <repository-url>` this repository
* change into the new directory
* `bundle install`

## Running / Development

* `rails server`
* Visit your app at [http://localhost:3000](http://localhost:3000).

### Code Generators

Make use of the many generators for code, try `rails generate --help` for more details

### Running Tests

* `tbd`

### Building

* `tbd` (development)
* `tbd` (production)

### Deploying

tbd

## Further Reading / Useful Links

tbd

class PurchaseOrder < ActiveRecord::Base
  belongs_to :shipping_route
  belongs_to :shipping_address, foreign_key: 'level_3'
  has_many :purchase_positions

  has_many :purchase_order_pallet_assignments
  has_many :pallets, through: :purchase_order_pallet_assignments
end

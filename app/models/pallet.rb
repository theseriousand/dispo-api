class Pallet < ActiveRecord::Base
  has_many :line_items

  has_many :variants, through: :line_items
  has_many :purchase_positions, through: :variants

end

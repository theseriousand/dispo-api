class LineItem < ActiveRecord::Base
  self.table_name = 'pallet_purchase_position_assignments'

  belongs_to :pallet
  belongs_to :purchase_position
  belongs_to :variant
end

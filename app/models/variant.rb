class Variant < ActiveRecord::Base
  belongs_to :purchase_position, primary_key: 'baan_id', foreign_key: 'purchase_position_id'
  belongs_to :purchase_order, primary_key: 'baan_id', foreign_key: 'purchase_order_id'

  belongs_to :commodity_code, primary_key: 'code', foreign_key: 'commodity_code_id'
  belongs_to :shipping_route, primary_key: 'name', foreign_key: 'shipping_route_id'
  belongs_to :zip_location, primary_key: 'title', foreign_key: 'zip_location_id'

  belongs_to :shipping_address, class_name: 'Address', primary_key: 'code', foreign_key: 'level_3'
end

class PalletsController < ApplicationController
  def index
    @pallets = Pallet.includes(:purchase_positions, :line_items, :variants)
    if params[:per_page] || params[:page]
      @pallets = @pallets.page(params[:page]).per(params[:per_page])
    end
    @pallets = @pallets.where(id: params[:q][:id]) if params[:q] && params[:q][:id].present?

    respond_to do |format|
      format.json { render json: @pallets, meta: { total_pages: @pallets.total_pages } }
    end
  end

  def show
    @pallet = Pallet.includes(line_items: [:variant, :purchase_position]).where(id: params[:id]).first!
    respond_to do |format|
      format.json { render json: @pallet }
    end
  end

  def destroy
    @pallet = Pallet.includes(line_items: [:variant, :purchase_position]).where(id: params[:id]).first!
    @pallet.destroy

    respond_to do |format|
      format.json { render json: @pallet }
    end
  end
end

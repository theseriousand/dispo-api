class LineItemsController < ApplicationController
  def index
    @line_items = LineItem.includes(:variant, purchase_position: :variants)
    @line_items = @line_items.where(id: params[:ids]) if params[:ids]

    respond_to do |format|
      format.json { render json: @line_items }
    end
  end

  def show
    @line_item = LineItem.includes(:variant, :purchase_position).where(id: params[:id]).first!
    respond_to do |format|
      format.json { render json: @line_item }
    end
  end

  def update
    @line_item = LineItem.includes(:variant, :purchase_position).where(id: params[:id]).first!
    @purchase_position = @line_item.purchase_position

    @line_item.attributes = line_item_params

    if @line_item.quantity_changed?
      was = @line_item.quantity_was
      current = @line_item.quantity

      if was < current
        @purchase_position.available_quantity -= current - was
      else
        @purchase_position.available_quantity += was - current
      end
      @purchase_position.save
    end
    @line_item.save

    respond_to do |format|
      format.json { render json: @line_item }
    end
  end

  def destroy
    @line_item = LineItem.includes(:variant, :purchase_position).where(id: params[:id]).first!
    @purchase_position = @line_item.purchase_position

    @purchase_position.available_quantity += @line_item.quantity

    @purchase_position.save
    @line_item.destroy

    respond_to do |format|
      format.json { render json: @line_item }
    end
  end

  private

  def line_item_params
    params.require(:line_item).permit(:quantity)
  end
end

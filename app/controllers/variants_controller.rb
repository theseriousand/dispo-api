class VariantsController < ApplicationController
  def index
    @variants = Variant.includes(:purchase_position).limit(100)
    respond_to do |format|
      format.json { render json: @variants }
    end
  end

  def show
    @variant = Variant.includes(:purchase_position).where(id: params[:id]).first!
    respond_to do |format|
      format.json { render json: @variant }
    end
  end
end

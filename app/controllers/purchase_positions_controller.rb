class PurchasePositionsController < ApplicationController
  def index
    @purchase_positions = PurchasePosition.includes(:zip_location, :variants).order('delivery_date DESC').page(params[:page]).per(params[:per_page])
    respond_to do |format|
      format.json { render json: @purchase_positions, meta: { total_pages: @purchase_positions.total_pages } }
    end
  end

  def show
    @purchase_position = PurchasePosition.where(id: params[:id]).first!
    respond_to do |format|
      format.json { render json: @purchase_position }
    end
  end
end

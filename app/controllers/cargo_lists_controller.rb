class PurchaseOrdersController < ApplicationController
  def index
    @cargo_lists = PurchaseOrder.includes(:pallets, :shipping_route, :shipping_address, purchase_positions: [:variants]).order('delivery_date DESC').page(params[:page]).per(params[:per_page]).select(:id, :baan_id, :shipping_route_id, :level_3, :delivery_date)
    @cargo_lists = @purchase_orders.where(delivered: false)

    respond_to do |format|
      format.json { render json: @purchase_orders, meta: { total_pages: @purchase_orders.total_pages } }
    end
  end

  def show
    @purchase_order = PurchaseOrder.includes(:pallets, :purchase_positions).where(id: params[:id]).first!
    respond_to do |format|
      format.json { render json: @purchase_order }
    end
  end
end

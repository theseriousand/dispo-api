class LineItemSerializer < ActiveModel::Serializer
  attributes :id, :purchase_position_id, :variant_id, :pallet_id, :quantity

  has_one :purchase_position
  # has_one :pallet
  # has_one :variant
end

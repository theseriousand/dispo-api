class VariantSerializer < ActiveModel::Serializer
  attributes :id, :purchase_position_id
end

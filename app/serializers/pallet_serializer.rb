class PalletSerializer < ActiveModel::Serializer
  attributes :id, :purchase_position_ids, :variant_ids, :line_item_ids

  def purchase_position_ids
    object.purchase_positions.collect(&:id)
  end

  def variant_ids
    object.variants.collect(&:id)
  end

  def line_item_ids
    object.line_items.collect(&:id)
  end
end

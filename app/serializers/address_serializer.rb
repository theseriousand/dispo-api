class AddressSerializer < ActiveModel::Serializer
  attributes :id, :consignee_full

  # has_many :purchase_orders

  def consignee_full
    "#{object.company_name}, #{object.street}, #{object.country}-#{object.postal_code} #{object.city}"
  end
end

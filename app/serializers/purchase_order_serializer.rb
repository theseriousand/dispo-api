class PurchaseOrderSerializer < ActiveModel::Serializer
  attributes :id, :baan_id, :delivery_date, :purchase_position_ids, :pallet_ids

  has_one :shipping_route
  has_one :shipping_address

  def purchase_position_ids
    object.purchase_positions.collect(&:id)
  end

  def pallet_ids
    object.pallets.collect(&:id)
  end
end

class PurchasePositionSerializer < ActiveModel::Serializer
  attributes :id, :baan_id, :delivery_date, :article, :storage_location, :available_quantity, :variant_ids
  has_one :zip_location

  def variant_ids
    object.variants.collect(&:id)
  end
end
